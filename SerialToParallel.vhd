library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;


entity SerialToParallel is
port (
	Serial, SCLK, SBYTE: in STD_LOGIC;
	Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, BCLK, PBYTE: out STD_LOGIC
	);
end SerialToParallel;


architecture Deserialization of SerialToParallel is
	signal B0, B1, B2, B3, B4, B5, B6, B7 : STD_LOGIC;		-- Bits Buffer
begin

	process (SCLK, SBYTE, BCLK) is
	variable State : integer;
	variable SyncByte : boolean := false;

	begin
		if rising_edge(SBYTE) then
			State := 0;						-- Sync Serial Packet
			SyncByte := true;
		end if;
		
		if falling_edge(SCLK) then
			if (State = 0) then				-- bit 0
				B0 <= (Serial);
				State := 1;
			elsif (State = 1) then
				B1 <= (Serial);
				State := 2;
			elsif (State = 2) then
				B2 <= (Serial);
				State := 3;
			elsif (State = 3) then
				B3 <= (Serial);
				State := 4;
			elsif (State = 4) then
				B4 <= (Serial);
				State := 5;
			elsif (State = 5) then
				B5 <= (Serial);
				State := 6;
			elsif (State = 6) then
				B6 <= (Serial);
				State := 7;
			elsif (State = 7) then
				B7 <= (Serial);		
				State := 0;
			end if;
		end if;
		
		if rising_edge(SCLK) then
			if (State = 0) then
				BCLK <= '0';
			elsif (State = 4) then
				BCLK <= '1';
			end if;
		end if;
		
		if falling_edge(BCLK) then
			if (SyncByte) then
					PBYTE <= '1';
					SyncByte := false;
				else
					PBYTE <= '0';
			end if;
				
			Q0 <= (B0);
			Q1 <= (B1);
			Q2 <= (B2);
			Q3 <= (B3);
			Q4 <= (B4);
			Q5 <= (B5);
			Q6 <= (B6);
			Q7 <= (B7);
	
		end if;
		
	end process;
	
end Deserialization;
