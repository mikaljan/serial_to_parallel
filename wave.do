onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_serialtoparallel/t_Serial
add wave -noupdate /tb_serialtoparallel/t_SCLK
add wave -noupdate /tb_serialtoparallel/t_SBYTE
add wave -noupdate /tb_serialtoparallel/t_Q0
add wave -noupdate /tb_serialtoparallel/t_Q1
add wave -noupdate /tb_serialtoparallel/t_Q2
add wave -noupdate /tb_serialtoparallel/t_Q3
add wave -noupdate /tb_serialtoparallel/t_Q4
add wave -noupdate /tb_serialtoparallel/t_Q5
add wave -noupdate /tb_serialtoparallel/t_Q6
add wave -noupdate /tb_serialtoparallel/t_Q7
add wave -noupdate /tb_serialtoparallel/t_BCLK
add wave -noupdate /tb_serialtoparallel/t_PBYTE
add wave -noupdate /tb_serialtoparallel/clk_period
add wave -noupdate -expand /tb_serialtoparallel/t_Din
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 200
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {17831 ns} {18784 ns}
