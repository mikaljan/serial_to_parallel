library IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_unsigned.all;
USE IEEE.std_logic_arith.all;

entity tb_SerialToParallel is
end tb_SerialToParallel;

architecture tb of tb_SerialToParallel is
      signal t_Serial : std_logic := '0';
      signal t_SCLK : std_logic := '0';
      signal t_SBYTE : std_logic;
      signal t_Q0 : std_logic;
      signal t_Q1 : std_logic;
      signal t_Q2 : std_logic;
      signal t_Q3 : std_logic;
      signal t_Q4 : std_logic;
      signal t_Q5 : std_logic;
      signal t_Q6 : std_logic;
      signal t_Q7 : std_logic;
      signal t_BCLK : std_logic := '0';
      signal t_PBYTE : std_logic := '0';
      constant clk_period : time := 10 ns;
      constant t_Din : std_logic_vector ( 7 downto 0) := "11111111";
   
    component SerialToParallel
	port (
	Serial, SCLK, SBYTE: in STD_LOGIC;
	Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, BCLK, PBYTE: out STD_LOGIC
	);
      end component SerialToParallel;
       
begin
    test_SerialToParallel : SerialToParallel port map(
            Serial => t_Serial,
            SCLK => t_SCLK,
            SBYTE => t_SBYTE,
            Q0 => t_Q0,
            Q1 => t_Q1,
            Q2 => t_Q2,
            Q3 => t_Q3,
            Q4 => t_Q4,
            Q5 => t_Q5,
            Q6 => t_Q6,
            Q7 => t_Q7  );
              
    t_SCLK <= not t_SCLK after clk_period/2;      

     
   
--      main_proc : process     ( t_SCLK )                    
--        variable index : integer := 0;
--            begin
--           
--            if ( t_SCLK'event and t_SCLK = '1' ) then
--            if ( index < 8 ) then   
--                t_Din <= Serial(index);
--                index := index + 1;
--            end if;
--        end if;              
--    end process; 


end tb;